<?php
/**
 * Description
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2016 Denis Chenu <http://www.sondages.pro>
 * @license WTFPL/LPRAB
 * @version 0.0.1
 *
 * You just DO WHAT THE FUCK YOU WANT TO.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
class showYiiLog extends \ls\pluginmanager\PluginBase
{

  protected $storage = 'DbStorage';

  static protected $name = 'showYiiLog';
  static protected $description = 'Just add a margin too yiilog table';

  public function init() {
    $this->subscribe('beforeControllerAction', 'cssYiiLog');
  }
  public function cssYiiLog()
  {
    if(App()->getConfig("debug")>0)
    {
      Yii::app()->clientScript->registerCss("css".get_class($this)."_plugin".$this->id,".yiiLog{margin-bottom:70px}");// 70px is fro amin part
    }
  }
}
